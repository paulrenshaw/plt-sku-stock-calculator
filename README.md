# Pretty Little Thing SKU Stock Calculator

Calculates current stock for a given SKU from a list of stock items and related transactions.

Test files are included within the src subdirectory that contains the functions they are used to test.

## Setup
```
npm install
```

## Test
```
npm run test
```

## Build
```
npm run build
```