import getProduct from './products/getSingle'
import calculateStock from './products/calculateStock'
import getTransactions from './transactions/getList'
import validateSku from './utils/validateSku'

import stockImport from './stock.json'
import transactionsImport from './transactions.json'

const _stock: Product[] = stockImport.map(product => {
  const { sku, stock } = product
  return {
    sku: sku as string,
    stock: Number(stock)
  }
})

const _transactions: Transaction[] = transactionsImport.map(transaction => {
  const { qty, sku, type } = transaction
  return {
    qty: Number(qty), 
    sku: sku as string, 
    type: type as "order" | "refund"
  }
})

export default async (sku: string): Promise<{ sku: string; stock: number }> => {
  
  await validateSku(sku)

  const productP = getProduct(_stock, sku)
  const transactionsP = getTransactions(_transactions, sku)
  
  let [product, transactions] = await Promise.all([productP, transactionsP])
  
  product.stock = await calculateStock(product, transactions)

  return product

}
