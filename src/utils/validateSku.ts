import handleError from './handleError'

/**
 * Checks whether sku is a non-empty string
 */
export default async (sku: string): Promise<void> => {
  if (!sku || typeof sku !== 'string') handleError({ message: "sku must be a non-empty string", data: sku })
  return 
}
