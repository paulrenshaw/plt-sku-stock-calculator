import handleError from './handleError'

describe('handleError', () => {
  it('throws', () => {
    const error = {
      message: "Error messsage."
    }
    expect(() => handleError(error)).toThrow(JSON.stringify(error))
  })
})
