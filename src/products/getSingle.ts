import handleError from '../utils/handleError'

/**
 * Returns a promise which resolve to a Product object.
 * 
 * Filters an array of Products and returns a Product matching the provided sku. 
 * If no products are found in the supplied list it returns a new Product with the supplied sku and stock value 0. 
 * If more than one product is found with the same sku an error is thrown with an array of matching products in the error data.  
 */
export default async (products: Product[], sku: string): Promise<Product> => {
  const matches = products.filter(item => item.sku === sku)

  if (matches.length > 1) {
    handleError({
      message: `More than one stock item found with sku: ${sku}`,
      data: matches
    })
  }

  let product: Product = {
    sku,
    stock: 0
  }
  
  if (matches.length === 1) {
    product = matches[0]
  }

  return product
}
