import getProduct from './getSingle'
import calculateStock from './calculateStock'

describe('products/getProduct', () => {
  it('resolves with existing product matching sku', async () => {
    const _stock: Product[] = [
      { sku: "sku", stock: 1 }
    ]
    const sku = 'sku'
    await expect(getProduct(_stock, sku)).resolves.toStrictEqual(_stock[0])
  })
  
  it('resolves with new product matching sku with stock value 0 when no existing stock items match the provided sku', async () => {
    const _stock: Product[] = [
      { sku: "sku", stock: 1 }
    ]
    const sku = 'missingSku'
    await expect(getProduct(_stock, sku)).resolves.toStrictEqual({ sku, stock: 0 })
  })
  
  it('rejects when more than one product matches sku', async () => {
    const _stock: Product[] = [
      { sku: "sku", stock: 1 },
      { sku: "sku", stock: 2 }
    ]
    const sku = 'sku'
    await expect(() => getProduct(_stock, sku)).rejects.toThrow()
  })
})

describe('products/calculateStock', () => {
  it('correctly calculates product stock based on current stock value and transactions qty and type', async () => {
    const product: Product = { sku: 'sku', stock: 1 }
    const transactions: Transaction[] = [
      { qty: 2, sku: product.sku, type: 'order'},
      { qty: 1, sku: product.sku, type: 'refund'}
    ]
    const expected = product.stock - 2 + 1
    const result = await calculateStock(product, transactions)
    expect(result).toEqual(expected)
  })

  // I was going to add a test to check that calculateStock throws if a transaction is passed with an invalid type property
  // But since I'm using typescript for tests as well as the source code, I can't even define an invalid transaction
  // or pass an object literal to the calculateStock function which does not match the Transaction type
  // i.e. the test code itself would be invalid if I tried to do this
})
