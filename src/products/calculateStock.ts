import handleError from '../utils/handleError'

/**
 * Returns a promise that resolves to an integer representing the current stock value.
 * 
 * Calculates the current stock value 
 * from the original product stock value 
 * and the provided qty and type of transactions matching the product sku
 */
export default async (product: Product, transactions: Transaction[]): Promise<number> => {

  let stock = product.stock || 0
  
  transactions.forEach(transaction => {
    if (transaction.sku === product.sku) {
      switch(transaction.type) {
        case 'order':
          stock -= transaction.qty
          break
        case 'refund':
          stock += transaction.qty
          break
        default:
          handleError({ message: `Invalid transaction type: ${transaction.type}`, data: transaction })
      }
    }
  })

  return stock

}
