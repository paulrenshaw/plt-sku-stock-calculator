interface CustomError {
  message: string
  code?: string
  data?: any
  err?: any
}

interface Product {
  sku: string
  stock: number
}

interface Transaction {
  qty: number
  sku: string
  type: "order" | "refund"
}
