/**
 * Returns a promise which resolves to an Array of transactions matching the provided sku.
 */
export default async (transactions: Transaction[], sku: string): Promise<Transaction[]> => {
  return transactions.filter(transaction => transaction.sku === sku)
}