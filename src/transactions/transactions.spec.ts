import getTransactions from './getList'

const transactions: Transaction[] = [
  { qty: 1, sku: 'sku1', type: 'order' },
  { qty: 2, sku: 'sku1', type: 'refund'},
  { qty: 3, sku: 'sku2', type: 'order' }
]

describe('transactions/getList', () => {
  it('resolves with an array of transactions matching sku', async() => {
    const sku = 'sku1'
    const result = [
      { qty: 1, sku: 'sku1', type: 'order' },
      { qty: 2, sku: 'sku1', type: 'refund'}
    ]
    let matching = await getTransactions(transactions, sku)
    expect(matching).toStrictEqual(result)
  })

  it('resolves with an empty array when sku does not match any transactions', async () => {
    const sku = 'sku3'
    let matching = await getTransactions(transactions, sku)
    expect(matching).toStrictEqual([])
  })
})
